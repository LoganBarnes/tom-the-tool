# Tom the Tool

### [PLAY](https://loganbarnes.gitlab.io/tom-the-tool)

## Local Development

```bash
git clone https://gitlab.com/LoganBarnes/tom-the-tool.git
cd tom-the-tool
npm i
npm run dev
```

View on `localhost:8080`. Edit and save files for hot reload.
