import SharedGameData from "@/engine/SharedGameData";

class GameRunner {
  private prevTimeMillis: number;
  private accumulator: number;
  private gameData: SharedGameData;

  private updateFunction: (gameData: SharedGameData) => void;
  private renderFunction: (gameData: SharedGameData) => void;

  constructor(
    updateFunction: (gameData: SharedGameData) => void,
    renderFunction: (gameData: SharedGameData) => void
  ) {
    this.prevTimeMillis = 0;
    this.accumulator = 0;
    this.gameData = new SharedGameData(false, 0, 0, this);
    this.updateFunction = updateFunction;
    this.renderFunction = renderFunction;
  }

  // tims is assumed to be in seconds unless specified
  loop(newTimeMillis: number): void {
    const timeDiff = (newTimeMillis - this.prevTimeMillis) * 1e-3; // ms to s
    const frameTime: number = Math.min(timeDiff, 0.1);
    this.prevTimeMillis = newTimeMillis;

    if (!this.gameData.paused) {
      this.accumulator += frameTime;

      while (this.accumulator >= this.gameData.timeStep) {
        this.gameData.worldTime += this.gameData.timeStep;
        this.accumulator -= this.gameData.timeStep;
        this.updateFunction(this.gameData);
      }
    }

    this.gameData.renderAlpha = this.accumulator / this.gameData.timeStep;
    this.renderFunction(this.gameData);

    if (!this.gameData.paused) {
      window.requestAnimationFrame(this.loop.bind(this));
    }
  }

  runLoop() {
    if (!this.gameData.paused) {
      this.prevTimeMillis = window.performance.now();
      this.loop(window.performance.now());
    }
  }

  get sharedGameData(): SharedGameData {
    return this.gameData;
  }
}

export default GameRunner;
