import { glMatrix, vec2, vec3, mat4 } from "gl-matrix";
import defaultVert from "ts-shader-loader!@/assets/shaders/default.vert";
import defaultFrag from "ts-shader-loader!@/assets/shaders/default.frag";
import {
  GLUtils,
  GLProgram,
  GLBuffer,
  GLVertexArray,
  Camera
} from "ts-graphics";
import Tool from "@/game/Tool";
// import { CutterType, ToolParams } from "@/game/ToolParams";

function clamp(val: number, min: number, max: number) {
  return Math.max(min, Math.min(max, val));
}

function lerp(x: number, y: number, a: number): number {
  return x * (1.0 - a) + y * a;
}

class IntersectData {
  orientedStart: vec2 = vec2.create();
  orientedEnd: vec2 = vec2.create();
  xFullRange: vec2 = vec2.create();
  yOffset: number = 0;
  xMiddleRange: vec2 = vec2.create();
  middleRangeRange: number = 0;
  iterStart: number = 0;
  iterEnd: number = 0;
}

class CrashData {
  private _crashHeight: number;

  constructor(crashHeight: number) {
    this._crashHeight = crashHeight;
  }

  get crashHeight(): number {
    return this._crashHeight;
  }
}

/**
 *
 */
class Stock {
  // OpenGL
  private glUtils: GLUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private modelMatrix: mat4;
  private color: vec3;
  private pausedColor: vec3;
  private vboData: Float32Array;

  private stockBoundsX: vec2;
  private stockBoundsY: vec2;
  private divisions: number;

  constructor(glUtils: GLUtils) {
    this.glUtils = glUtils;
    this.modelMatrix = mat4.create();

    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        text: defaultVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: defaultFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    this.stockBoundsX = vec2.fromValues(-150, 150);
    this.stockBoundsY = vec2.fromValues(-150, -20);

    const stockLengthX = this.stockBoundsX[1] - this.stockBoundsX[0];

    // Triangle strip of top and bottom points
    this.divisions = 1201;
    this.vboData = new Float32Array(this.divisions * 4);

    let x_pos: number;
    for (let i: number = 0; i < this.divisions; ++i) {
      x_pos = (i / (this.divisions - 1)) * stockLengthX + this.stockBoundsX[0];

      this.vboData[i * 4 + 0] = x_pos;
      this.vboData[i * 4 + 1] = this.stockBoundsY[1];

      this.vboData[i * 4 + 2] = x_pos;
      this.vboData[i * 4 + 3] = this.stockBoundsY[0];
    }

    this.vbo = this.glUtils.createVbo(this.vboData);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);

    this.color = vec3.fromValues(0.5, 0.5, 1.0);
    this.pausedColor = vec3.fromValues(0.2, 0.2, 0.2);
  }

  /**
   * Update the stock based on the current tool position
   *
   * @param tool
   */
  update(tool: Tool): null | CrashData {
    const data: IntersectData = this.computeIntersectData(tool);

    let x: number;
    let height: null | number;

    for (let i: number = data.iterStart; i <= data.iterEnd; ++i) {
      x = this.vboData[i * 4 + 0];

      height = this.computeHeight(x, data, tool);

      if (height) {
        this.vboData[i * 4 + 1] = Math.min(this.vboData[i * 4 + 1], height);
      }
    }

    this.resetVbo();

    return null;
  }

  /**
   *
   * @param camera
   * @param alpha
   * @param paused
   */
  render(camera: Camera, alpha: number, paused: boolean): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program.use(
      (): void => {
        this.program.setMatrixUniform(
          camera.screenFromWorldMatrix,
          "screenFromWorld"
        );

        this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");

        if (paused) {
          this.program.setFloatUniform(this.pausedColor, "color");
        } else {
          this.program.setFloatUniform(this.color, "color");
        }
        this.vao.render(gl.TRIANGLE_STRIP, 0, this.vboData.length / 2);
      }
    );
  }

  /**
   * Almost identical to {@link Stock#update} but returns {@link CrashData} if
   * any collisions are found instead of updating the heights.
   *
   * @param tool
   */
  checkVerticalCrash(
    xLeft: number,
    xRight: number,
    height: number
  ): null | CrashData {
    let iterStart: number;
    let iterEnd: number;
    [iterStart, iterEnd] = this.computeIterLocations(
      vec2.fromValues(xLeft, xRight)
    );

    let x: number;
    let stockHeight: number;
    let highestCrashHeight: null | number = null;

    for (let i: number = iterStart; i <= iterEnd; ++i) {
      x = this.vboData[i * 4 + 0];

      stockHeight = this.vboData[i * 4 + 1];

      if (xLeft <= x && x <= xRight && height < stockHeight) {
        if (!highestCrashHeight || stockHeight > highestCrashHeight) {
          highestCrashHeight = stockHeight;
        }
      }
    }

    if (highestCrashHeight) {
      return new CrashData(highestCrashHeight);
    }

    return null;
  }

  /**
   *
   * @param tool
   */
  private computeIntersectData(tool: Tool): IntersectData {
    let data: IntersectData = new IntersectData();

    let movementDirection: vec2 = vec2.subtract(
      vec2.create(),
      tool.endPos,
      tool.startPos
    );
    vec2.normalize(movementDirection, movementDirection);

    vec2.copy(data.orientedStart, tool.startPos);
    vec2.copy(data.orientedEnd, tool.endPos);

    if (data.orientedStart[0] > data.orientedEnd[0]) {
      // swap
      [data.orientedStart, data.orientedEnd] = [
        data.orientedEnd,
        data.orientedStart
      ];
    } else if (glMatrix.equals(data.orientedStart[0], data.orientedEnd[0])) {
      data.orientedStart[1] = Math.min(
        data.orientedStart[1],
        data.orientedEnd[1]
      );
      data.orientedEnd[1] = data.orientedStart[1];
    }

    const radius: number = tool.params.diameter / 2.0;

    data.xFullRange[0] = data.orientedStart[0] - radius;
    data.xFullRange[1] = data.orientedEnd[0] + radius;

    data.yOffset = 0.0;
    let xOffset: number =
      radius *
      Math.sign(movementDirection[0]) *
      Math.sign(movementDirection[1]);

    if (xOffset === 0.0) {
      xOffset = radius;
    }

    data.xMiddleRange[0] = data.orientedStart[0] + xOffset;
    data.xMiddleRange[1] = data.orientedEnd[0] + xOffset;

    data.middleRangeRange = data.xMiddleRange[1] - data.xMiddleRange[0];

    [data.iterStart, data.iterEnd] = this.computeIterLocations(data.xFullRange);

    return data;
  }

  /**
   * Determine the indices into {@link Stock#vboData} that correspond to
   * the given world-space range.
   *
   * @param range - the world-space x-range
   */
  private computeIterLocations(range: vec2): number[] {
    const stockLengthX: number = this.stockBoundsX[1] - this.stockBoundsX[0];

    let iterStart: number = Math.floor(
      ((range[0] - this.stockBoundsX[0]) / stockLengthX) * (this.divisions - 1)
    );
    let iterEnd: number = Math.ceil(
      ((range[1] - this.stockBoundsX[0]) / stockLengthX) * (this.divisions - 1)
    );

    iterStart = Math.max(0, iterStart);
    iterEnd = Math.min(this.divisions - 1, iterEnd);

    return [iterStart, iterEnd];
  }

  /**
   *
   * @param x
   * @param data
   * @param tool
   */
  private computeHeight(
    x: number,
    data: IntersectData,
    tool: Tool
  ): null | number {
    if (x < data.xFullRange[0] || data.xFullRange[1] < x) {
      return null;
    }

    if (x <= data.xMiddleRange[0]) {
      return data.orientedStart[1];
    }

    if (x < data.xMiddleRange[1]) {
      let t: number = (x - data.xMiddleRange[0]) / data.middleRangeRange;
      t = clamp(t, 0, 1); // clamp to [0, 1]
      return lerp(data.orientedStart[1], data.orientedEnd[1], t) + data.yOffset;
    }

    return data.orientedEnd[1];
  }

  /**
   *
   */
  private resetVbo() {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.vbo = this.glUtils.createVbo(this.vboData);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);
  }
}

export { Stock, CrashData };
export default Stock;
