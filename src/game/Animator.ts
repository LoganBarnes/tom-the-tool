import { vec2, vec3, mat4, glMatrix } from "gl-matrix";
import defaultVert from "ts-shader-loader!@/assets/shaders/default.vert";
import defaultFrag from "ts-shader-loader!@/assets/shaders/default.frag";
import {
  GLUtils,
  GLProgram,
  GLBuffer,
  GLVertexArray,
  Camera
} from "ts-graphics";
import Tool from "@/game/Tool";
import Stock, { CrashData } from "@/game/Stock";
import Machine, { MachineState } from "@/game/Machine";

function clamp(val: number, min: number, max: number) {
  return Math.max(min, Math.min(max, val));
}

function lerp(x: number, y: number, a: number): number {
  return x * (1.0 - a) + y * a;
}

class Animator {
  // OpenGL
  private glUtils: GLUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private modelMatrix: mat4;
  private identityMatrix: mat4;
  private color: vec3;
  private pausedColor: vec3;
  private vboData: Float32Array;

  private rapidStage: number;
  private _rapidPos: vec2;
  private _toolChange: boolean;

  constructor(glUtils: GLUtils) {
    this.glUtils = glUtils;
    this.modelMatrix = mat4.create();
    this.identityMatrix = mat4.create();

    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        text: defaultVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: defaultFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    // Triangle strip of top and bottom points
    this.vboData = new Float32Array(4 * 2 + 15 * 2);

    for (let i: number = 0; i < 4; ++i) {
      this.vboData[i * 2 + 0] = 0;
      this.vboData[i * 2 + 1] = 0;
    }

    const radius: number = 1.5;

    let angle: number;
    for (let i: number = 0; i < 15; ++i) {
      angle = (i * Math.PI * 2.0) / 14;
      this.vboData[i * 2 + 8] = Math.cos(angle) * radius;
      this.vboData[i * 2 + 9] = Math.sin(angle) * radius;
    }

    this.vbo = this.glUtils.createVbo(this.vboData);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);

    this.color = vec3.fromValues(1.0, 0.0, 0.0);
    this.pausedColor = vec3.fromValues(0.5, 0.0, 0.0);

    this.rapidStage = 0;
    this._rapidPos = vec2.fromValues(0, 0);
    this._toolChange = false;
  }

  /**
   * Returns true if the tool needs to perform its normal update
   * (it isn't rapiding).
   *
   * @param tool
   * @param stock
   * @param timeStep
   * @param machine
   */
  update(
    tool: Tool,
    stock: Stock,
    timeStep: number,
    machine: Machine
  ): boolean {
    const rapidHeight: number = Math.max(
      this._rapidPos[1],
      Math.max(-10, tool.endPos[1])
    );

    if (this.rapidStage > 0) {
      let moveDir: vec2 = vec2.create();
      let newPos: vec2 = vec2.create();

      const rapidVel: number = 750;

      tool.startPos = vec2.copy(vec2.create(), tool.endPos);

      if (this.rapidStage === 1) {
        moveDir[1] = Math.sign(rapidHeight - tool.endPos[1]) * rapidVel;
        vec2.scaleAndAdd(newPos, tool.endPos, moveDir, timeStep);
        if (
          newPos[1] > rapidHeight ||
          glMatrix.equals(rapidHeight, newPos[1])
        ) {
          newPos[1] = rapidHeight;
          this.rapidStage = 2;
        }
      } else if (this.rapidStage === 2) {
        moveDir[0] = Math.sign(this._rapidPos[0] - tool.endPos[0]) * rapidVel;
        vec2.scaleAndAdd(newPos, tool.endPos, moveDir, timeStep);
        if (
          Math.sign(moveDir[0]) !== Math.sign(this._rapidPos[0] - newPos[0]) ||
          glMatrix.equals(newPos[0], this._rapidPos[0])
        ) {
          newPos[0] = this._rapidPos[0];
          this.rapidStage = 3;
        }
      } else {
        // this.rapidStage === 3
        moveDir[1] = Math.sign(this._rapidPos[1] - tool.endPos[1]) * rapidVel;
        vec2.scaleAndAdd(newPos, tool.endPos, moveDir, timeStep);
        if (
          Math.sign(moveDir[1]) !== Math.sign(this._rapidPos[1] - newPos[1]) ||
          glMatrix.equals(newPos[1], this._rapidPos[1])
        ) {
          newPos[1] = this._rapidPos[1];
          this.rapidStage = 4;
        }
      }

      tool.endPos = newPos;

      if (this.rapidStage >= 3 && !this.toolChange) {
        const radius: number = tool.params.diameter / 2.0;

        let crashData: null | CrashData = stock.checkVerticalCrash(
          newPos[0] - radius,
          newPos[0] + radius,
          newPos[1]
        );

        if (crashData) {
          newPos[1] = crashData.crashHeight;
        } else {
          // no crash with the tool, check the holder
          const holderRadius: number = tool.params.holderDiameter / 2.0;

          crashData = stock.checkVerticalCrash(
            newPos[0] - holderRadius,
            newPos[0] + holderRadius,
            newPos[1] + tool.params.shaftLength
          );

          if (crashData) {
            newPos[1] = crashData.crashHeight - tool.params.shaftLength;
          }
        }

        if (crashData) {
          // crash detected
          machine.state = MachineState.CRASH;
          this.toolChange = true; // rapidStage gets reset here

          tool.endPos = newPos;
          tool.startPos = vec2.copy(vec2.create(), tool.endPos);
        } else if (this.rapidStage === 4) {
          this.rapidStage = 0;
        }
      }
    }

    this.vboData[0] = tool.endPos[0];
    this.vboData[1] = tool.endPos[1];

    this.vboData[2] = tool.endPos[0];
    this.vboData[3] = rapidHeight;

    this.vboData[4] = this._rapidPos[0];
    this.vboData[5] = rapidHeight;

    this.vboData[6] = this._rapidPos[0];
    this.vboData[7] = this._rapidPos[1];

    this.resetVbo();

    return this.rapidStage == 0;
  }

  /**
   *
   * @param camera
   * @param alpha
   * @param paused
   */
  render(camera: Camera, alpha: number, paused: boolean): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    mat4.fromTranslation(
      this.modelMatrix,
      vec3.fromValues(this._rapidPos[0], this._rapidPos[1], 0)
    );

    this.program.use(
      (): void => {
        this.program.setMatrixUniform(
          camera.screenFromWorldMatrix,
          "screenFromWorld"
        );

        this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");

        if (paused) {
          this.program.setFloatUniform(this.pausedColor, "color");
        } else {
          this.program.setFloatUniform(this.color, "color");
        }
        this.vao.render(gl.TRIANGLE_FAN, 4, 15);

        this.program.setMatrixUniform(this.identityMatrix, "worldFromLocal");
        this.vao.render(gl.LINE_STRIP, 0, 4);
      }
    );
  }

  set rapidPos(rapidPos: vec2) {
    if (this.rapidStage === 0) {
      this._rapidPos = rapidPos;
      this._rapidPos[1] = clamp(this._rapidPos[1], -150, 50);
    }
  }

  set toolChange(toolChange: boolean) {
    this._toolChange = toolChange;
    if (this._toolChange) {
      vec2.set(this._rapidPos, 0, 0);
      this.rapidStage = 1;
    }
  }

  rapid(): void {
    if (this.rapidStage === 0) {
      this.rapidStage = 1;
    }
  }

  private resetVbo() {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.vbo = this.glUtils.createVbo(this.vboData);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);
  }
}

export default Animator;
