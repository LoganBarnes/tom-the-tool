import { vec2, vec3, mat4 } from "gl-matrix";
import defaultVert from "ts-shader-loader!@/assets/shaders/default.vert";
import defaultFrag from "ts-shader-loader!@/assets/shaders/default.frag";
import {
  GLUtils,
  GLProgram,
  GLBuffer,
  GLVertexArray,
  Camera
} from "ts-graphics";
import { CutterType, ToolParams } from "@/game/ToolParams";

class Tool {
  // OpenGL
  private glUtils: GLUtils;
  private program: GLProgram;
  private vbo: GLBuffer;
  private vao: GLVertexArray;
  private modelMatrix: mat4;
  private numVerts: number;
  private toolColor: vec3;
  private pausedToolColor: vec3;

  // Movement
  private _movingRight: boolean;
  private _movingLeft: boolean;
  private _movingUp: boolean;
  private _movingDown: boolean;

  private _prevPos: vec2;
  private _currPos: vec2;
  private _vel: vec2;

  // Tool
  private _params: ToolParams;

  constructor(
    glUtils: GLUtils,
    params: ToolParams,
    initialPos: vec2 = vec2.fromValues(0, 0)
  ) {
    this.glUtils = glUtils;
    this.modelMatrix = mat4.create();

    const gl: WebGLRenderingContext = this.glUtils.getContext();

    this.program = this.glUtils.createProgram(
      {
        text: defaultVert,
        type: gl.VERTEX_SHADER
      },
      {
        text: defaultFrag,
        type: gl.FRAGMENT_SHADER
      }
    );

    // Tool with holder
    const toolRadius: number = params.diameter / 2;
    const holderRadius: number = params.holderDiameter / 2;

    const data: Float32Array = new Float32Array([
      0,
      params.shaftLength + params.holderLength,
      -holderRadius,
      params.shaftLength + params.holderLength,
      -holderRadius,
      params.shaftLength,
      -toolRadius,
      params.shaftLength,
      -toolRadius,
      0,
      toolRadius,
      0,
      toolRadius,
      params.shaftLength,
      holderRadius,
      params.shaftLength,
      holderRadius,
      params.shaftLength + params.holderLength
    ]);
    this.numVerts = data.length / 2;

    this.vbo = this.glUtils.createVbo(data);

    this.vao = this.glUtils.createVao(this.program, this.vbo, 0, [
      {
        name: "localPosition",
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ]);

    this.toolColor = vec3.fromValues(1.0, 0.7, 0.7);
    this.pausedToolColor = vec3.fromValues(0.3, 0.3, 0.3);

    this._movingLeft = false;
    this._movingRight = false;
    this._movingDown = false;
    this._movingUp = false;

    this._prevPos = vec2.copy(vec2.create(), initialPos);
    this._currPos = vec2.copy(vec2.create(), initialPos);
    this._vel = vec2.fromValues(0, 0);

    this._params = params;
  }

  /**
   * Non-rapid tool movement (no collision detection)
   *
   * @param worldTime
   * @param timeStep
   */
  update(worldTime: number, timeStep: number): void {
    vec2.copy(this._prevPos, this._currPos);

    let moveDir: vec2 = vec2.create();

    if (this._movingLeft) {
      moveDir[0] -= 1;
    }
    if (this._movingRight) {
      moveDir[0] += 1;
    }

    if (this._movingDown) {
      moveDir[1] -= 1;
    }
    if (this._movingUp) {
      moveDir[1] += 1;
    }

    moveDir[0] *= this._params.horizontalFeed;
    moveDir[1] *= this._params.verticalFeed;

    vec2.scaleAndAdd(this._currPos, this._currPos, moveDir, timeStep);
    this._currPos[1] = Math.max(-150, this._currPos[1]);
    this._currPos[1] = Math.min(50, this._currPos[1]);
  }

  render(camera: Camera, alpha: number, paused: boolean): void {
    const gl: WebGLRenderingContext = this.glUtils.getContext();

    const pos: vec2 = vec2.lerp(
      vec2.create(),
      this._prevPos,
      this._currPos,
      alpha
    );
    mat4.fromTranslation(this.modelMatrix, vec3.fromValues(pos[0], pos[1], 0));

    this.program.use(
      (): void => {
        this.program.setMatrixUniform(
          camera.screenFromWorldMatrix,
          "screenFromWorld"
        );

        this.program.setMatrixUniform(this.modelMatrix, "worldFromLocal");

        if (paused) {
          this.program.setFloatUniform(this.pausedToolColor, "color");
        } else {
          this.program.setFloatUniform(this.toolColor, "color");
        }
        this.vao.render(gl.TRIANGLE_FAN, 0, this.numVerts);
        // this.vao.render(gl.LINE_STRIP, 0, this.numVerts);
      }
    );
  }

  set movingRight(moving: boolean) {
    this._movingRight = moving;
  }

  set movingLeft(moving: boolean) {
    this._movingLeft = moving;
  }

  set movingUp(moving: boolean) {
    this._movingUp = moving;
  }

  set movingDown(moving: boolean) {
    this._movingDown = moving;
  }

  set startPos(startPos: vec2) {
    this._prevPos = startPos;
  }

  set endPos(endPos: vec2) {
    this._currPos = endPos;
  }

  get startPos(): vec2 {
    return this._prevPos;
  }

  get endPos(): vec2 {
    return this._currPos;
  }

  get params(): ToolParams {
    return this._params;
  }
}

export default Tool;
