enum CutterType {
  FLAT,
  BALL,
  CHAMFER,
  NUM_TYPES
}

class ToolParams {
  private _horizontalFeed: number;
  private _verticalFeed: number;
  private _cutterType: CutterType;
  private _fluteLength: number;
  private _shaftLength: number;
  private _diameter: number;
  private _holderLength: number;
  private _holderDiameter: number;

  constructor(
    horizontalFeed: number,
    verticalFeed: number,
    cutterType: CutterType,
    fluteLength: number,
    shaftLength: number,
    diameter: number,
    holderLength: number,
    holderDiameter: number
  ) {
    this._horizontalFeed = horizontalFeed;
    this._verticalFeed = verticalFeed;
    this._cutterType = cutterType;
    this._fluteLength = fluteLength;
    this._shaftLength = shaftLength;
    this._diameter = diameter;
    this._holderLength = holderLength;
    this._holderDiameter = holderDiameter;
  }

  get horizontalFeed(): number {
    return this._horizontalFeed;
  }

  get verticalFeed(): number {
    return this._verticalFeed;
  }

  get cutterType(): CutterType {
    return this._cutterType;
  }

  get fluteLength(): number {
    return this._fluteLength;
  }

  get shaftLength(): number {
    return this._shaftLength;
  }

  get diameter(): number {
    return this._diameter;
  }

  get holderLength(): number {
    return this._holderLength;
  }

  get holderDiameter(): number {
    return this._holderDiameter;
  }
}

export { CutterType, ToolParams };
