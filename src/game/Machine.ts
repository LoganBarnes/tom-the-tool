import { vec2, vec3, mat4 } from "gl-matrix";
import { GLUtils, Camera } from "ts-graphics";
import SharedGameData from "@/engine/SharedGameData";
import Tool from "@/game/Tool";
import Stock from "@/game/Stock";
import { ToolParams, CutterType } from "@/game/ToolParams";
import Animator from "@/game/Animator";
import { PopupVals } from "@/gui/Popup.vue";

enum MachineState {
  NORMAL,
  CRASH
}

class Machine {
  private glUtils: GLUtils;
  private gameData: SharedGameData;
  private popupVals: PopupVals;

  private _state: MachineState;

  private tool: Tool;
  private stock: Stock;
  private animator: Animator;

  constructor(
    glUtils: GLUtils,
    gameData: SharedGameData,
    popupVals: PopupVals
  ) {
    this.glUtils = glUtils;
    this.gameData = gameData;
    this.popupVals = popupVals;

    this._state = MachineState.NORMAL;

    this.tool = new Tool(
      this.glUtils,
      new ToolParams(
        /*horizontalFeed*/ 50,
        /*verticalFeed*/ 20,
        /*cutterType*/ CutterType.FLAT,
        /*fluteLength*/ 30,
        /*shaftLength*/ 50,
        /*diameter*/ 20,
        /*holderLength*/ 25,
        /*holderDiameter*/ 50
      )
    );

    this.stock = new Stock(this.glUtils);
    this.animator = new Animator(this.glUtils);
  }

  update(): void {
    if (
      this.animator.update(this.tool, this.stock, this.gameData.timeStep, this)
    ) {
      this.tool.update(this.gameData.worldTime, this.gameData.timeStep);
    }
    this.stock.update(this.tool);
  }

  render(): void {
    const cam: Camera = this.gameData.cameraMover.camera;
    this.stock.render(cam, this.gameData.renderAlpha, this.gameData.paused);
    this.tool.render(cam, this.gameData.renderAlpha, this.gameData.paused);
    this.animator.render(cam, this.gameData.renderAlpha, this.gameData.paused);
  }

  handleMousePress(event: MouseEvent, worldPos: vec2 | null): void {
    if (event.defaultPrevented) {
      return; // Do nothing if the event was already processed
    }

    this.resetState();

    if (worldPos) {
      this.animator.rapidPos = vec2.copy(vec2.create(), worldPos);
    }
    this.animator.rapid();

    event.preventDefault();
  }

  handleMouseRelease(event: MouseEvent): void {
    if (event.defaultPrevented) {
      return; // Do nothing if the event was already processed
    }

    event.preventDefault();
  }

  handleMouseMove(event: MouseEvent, worldPos: vec2 | null): void {
    if (worldPos) {
      this.animator.rapidPos = vec2.copy(vec2.create(), worldPos);
    }
  }

  handleKeyDown(event: KeyboardEvent): void {
    if (event.defaultPrevented) {
      return; // Do nothing if the event was already processed
    }

    switch (event.key) {
      case "ArrowDown":
      case "s":
        this.tool.movingDown = true;
        break;
      case "ArrowUp":
      case "w":
        this.tool.movingUp = true;
        break;
      case "ArrowLeft":
      case "a":
        this.tool.movingLeft = true;
        break;
      case "ArrowRight":
      case "d":
        this.tool.movingRight = true;
        break;
      case " ":
      case "Enter":
      case "Escape":
        this.resetState();
        break;
      default:
        return; // Quit when this doesn't handle the key event.
    }

    // Cancel the default action to avoid it being handled twice
    event.preventDefault();
  }

  handleKeyUp(event: KeyboardEvent): void {
    if (event.defaultPrevented) {
      return; // Do nothing if the event was already processed
    }

    switch (event.key) {
      case "ArrowDown":
      case "s":
        this.tool.movingDown = false;
        break;
      case "ArrowUp":
      case "w":
        this.tool.movingUp = false;
        break;
      case "ArrowLeft":
      case "a":
        this.tool.movingLeft = false;
        break;
      case "ArrowRight":
      case "d":
        this.tool.movingRight = false;
        break;
      default:
        return; // Quit when this doesn't handle the key event.
    }

    // Cancel the default action to avoid it being handled twice
    event.preventDefault();
  }

  resetState() {
    if (this._state === MachineState.CRASH) {
      this._state = MachineState.NORMAL;
      if (this.gameData.paused) {
        this.gameData.paused = false;
        this.popupVals.visible = false;
      }
    }
  }

  set state(state: MachineState) {
    this._state = state;
    if (this._state === MachineState.CRASH) {
      this.gameData.paused = true;
      this.popupVals.visible = true;
      this.popupVals.title = "CRASHED!";
      this.popupVals.message = "Tool change required";
      this.popupVals.altMsg = "(Click anywhere to resume)";
    }
  }
}

export { Machine, MachineState };
export default Machine;
